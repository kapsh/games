# Copyright 2016-2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=v${PV} ] \
    cmake \
    lua [ multibuild=false whitelist="5.1 5.2 5.3" ]

SUMMARY="Open source clone of Theme Hospital"
HOMEPAGE+=" http://corsixth.com"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    doc
"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen[dot] )
    build+run:
        dev-lua/lpeg[lua_abis:*(-)?]
        dev-lua/luafilesystem[lua_abis:*(-)?]
        media/ffmpeg
        media-libs/SDL:2[X]
        media-libs/SDL_mixer:2[midi]
        media-libs/freetype:2
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DLUA_INCLUDE_DIR:PATH=/usr/$(exhost --target)/include/lua${LUA_ABIS}
    -DLUA_LIBRARY:PATH=/usr/$(exhost --target)/lib/liblua${LUA_ABIS}.so
    -DLUA_PROGRAM_PATH:PATH=/usr/$(exhost --target)/bin/lua${LUA_ABIS}
    -DBUILD_ANIMVIEWER:BOOL=FALSE
    -DENABLE_UNIT_TESTS:BOOL=FALSE
    -DUSE_PRECOMPILED_DEPS:BOOL=FALSE
    -DUSE_SOURCE_DATADIRS:BOOL=FALSE
    -DUSE_VCPKG_DEPS:BOOL=FALSE
    -DWITH_AUDIO:BOOL=TRUE
    -DWITH_FREETYPE2:BOOL=TRUE
    -DWITH_LIBAV:BOOL=FALSE
    -DWITH_LUAJIT:BOOL=FALSE
    -DWITH_MOVIES:BOOL=TRUE
    -DWITH_VLD:BOOL=FALSE
)

src_compile() {
    default

    option doc && emake doc
}

src_install() {
    cmake_src_install

    if option doc; then
        insinto /usr/share/doc/${PNVR}/html
        doins -r doc/*
    fi
}

pkg_postinst() {
    elog "${PN} requires a copy of the original Theme Hospital resources."
    elog "See /usr/share/doc/${PNVR}/README.txt for more info."
}

