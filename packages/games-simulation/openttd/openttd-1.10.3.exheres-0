# Copyright 2009, 2010 Ingmar Vanhassel
# Copyright 2014 Alexander Scheuermann
# Distributed under the terms of the GNU General Public License v2

require flag-o-matic gtk-icon-cache

SUMMARY="An open source clone of Transport Tycoon Deluxe"
HOMEPAGE="https://www.openttd.org/"
DOWNLOADS="https://proxy.binaries.openttd.org/${PN}-releases/${PV}/openttd-${PV}-source.tar.xz"

UPSTREAM_DOCUMENTATION="https://wiki.openttd.org/Newbies [[ description = [ Tutorial ] lang = en ]]"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    dedicated [[ description = [ Build a dedicated server only binary ] ]]
    platform: amd64 x86
"

# No actual test target, but dry-run passes, causing make check to be run & break
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-arch/lzo:2
        app-arch/xz[>=5.0]
        sys-libs/zlib
        !dedicated? (
            dev-libs/icu:=[>=3.6.0]
            media-libs/fontconfig[>=2.3.0]
            media-libs/freetype:2
            media-libs/libpng:=[>=1.2]
            media-libs/SDL:2[>=2.0][X]
            x11-libs/libxdg-basedir[>=1.2]
        )
    suggestion:
        !dedicated? ( media-sound/timidity++ [[ description = [ Required for midi playback ] ]] )
    recommendation:
        !dedicated? ( games-simulation/openttd-opengfx[>=0.5] [[ description = [ OpenTTD requires atleast one graphics set to work ] ]] )
"

DEFAULT_SRC_COMPILE_PARAMS=( VERBOSE=1 )

src_configure() {
    local myconf=(
        --build=$(exhost --build)
        --host=$(exhost --target)
        --cc-build=$(exhost --build)-cc
        --cxx-build=$(exhost --build)-c++
        --pkg-config=${PKG_CONFIG}
        --CFLAGS_BUILD="$(print-build-flags CFLAGS)"
        --CXXFLAGS_BUILD="$(print-build-flags CXXFLAGS)"
        --LDFLAGS_BUILD="$(print-build-flags LDFLAGS)"

        --binary-dir=/usr/$(exhost --target)/bin
        --data-dir=/usr/share/${PN}
        --doc-dir=/usr/share/doc/${PNVR}
        --icon-dir=/usr/share/pixmaps
        --icon-theme-dir=/usr/share/icons/hicolor
        --install-dir="${IMAGE}"
        --man-dir=/usr/share/man/man6/
        --menu-dir=/usr/share/applications
        --prefix-dir=/

        --disable-strip
        --without-fluidsynth

    )

    if option dedicated; then
        myconf+=(
            --enable-dedicated
            --without-png
            --without-xdg-basedir
        )
    else
        myconf+=(
            --without-allegro
            --with-sdl=2
        )
    fi

    # --with-sse means build SSE2, SSSE3, SSE4.1 optimized code paths
    # usage is behind a cpuid check so there is no harm in enabling this
    # it can always be disabled via commandline option or config file
    if option platform:amd64 || option platform:x86; then
        myconf+=( --with-sse )
    else
        myconf+=( --without-sse )
    fi

    edo ./configure "${myconf[@]}"
}

